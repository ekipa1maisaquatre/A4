import csv
import logging
import os
from pathlib import Path
from typing import Generator

import matplotlib.pyplot as plt
import pandas as pd
import psycopg
from tqdm.auto import tqdm


def requete1(cur: psycopg.cursor):
    sql = """
    CREATE VIEW view_min_distance_par_dep AS
    SELECT DISTINCT (A.id), A.departement, M.point_2154 <-> A.point_2154  as dist
    FROM proj.markets M, proj.addresses A
    WHERE ST_DWithin(A.point_2154, M.point_2154, 100000)
    ORDER BY a.id, dist LIMIT 10000000;
    """
    cur.execute(sql)


def histogramme():
    sql_hist = pd.read_sql_query(
        """
    SELECT M_D.departement, AVG(distance_min)
    FROM view_min_distance M_D
    GROUP BY M_D.departement;
    """
    )
    departement = []
    dist = []
    for x, y in sql_hist.iterrows():
        departement.append(x)
        dist.append(y)
    plt.title("Moyenne des distances minimales par département")
    plt.hist(dist, departement)
