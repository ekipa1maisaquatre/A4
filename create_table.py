import csv
import logging
import os
from pathlib import Path
from typing import Generator

import matplotlib.pyplot as plt
import pandas as pd
import psycopg
from tqdm.auto import tqdm

logger = logging.getLogger(__name__)

LIMIT = 10_000_000


def create_schema(cur: psycopg.cursor) -> None:
    sql = """
        DROP SCHEMA IF EXISTS proj CASCADE;
        CREATE SCHEMA proj;

        CREATE TABLE proj.addresses (
            id BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
            code_insee VARCHAR(5) NOT NULL,
            departement VARCHAR(3),
            point_2154 Geometry(Point, 2154) NOT NULL CONSTRAINT geom_is_valid CHECK (
                ST_IsEmpty(point_2154) IS FALSE
                AND ST_SRID(point_2154) = 2154
                AND ST_IsValid(point_2154)
            )                        
        )
        ;
            
        CREATE TABLE proj.markets (
            id BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
            type VARCHAR(4),
            iris VARCHAR(10),
            dep VARCHAR(5),
            point_2154 Geometry(Point, 2154) NOT NULL CONSTRAINT geom_is_valid CHECK (
                ST_IsEmpty(point_2154) IS FALSE
                AND ST_SRID(point_2154) = 2154
                AND ST_IsValid(point_2154)
            )
        );

        
        """
    cur.execute(sql)


def get_markets() -> Generator[dict, None, None]:
    with Path(__file__).parent.joinpath("bpe20_ensemble_xy.csv").open(
        mode="rt", newline=""
    ) as f:
        reader = csv.DictReader(f, delimiter=";")
        for row in reader:
            yield row


def get_address_rows() -> Generator[dict, None, None]:
    with Path(__file__).parent.joinpath("adresses-france.csv").open(
        mode="rt", newline=""
    ) as f:
        reader = csv.DictReader(f, delimiter=";")
        for row in reader:
            yield row


def create_table():
    uri = os.getenv("PG_API0057_URI_RW")
    with psycopg.connect(uri, row_factory=psycopg.rows.dict_row) as conn:
        with conn.cursor() as cur:
            create_schema(cur)
            with cur.copy(
                "COPY proj.markets (type, iris, dep, point_2154) FROM STDIN"
            ) as copy:
                for row in tqdm(get_markets(), desc="Copying markets"):
                    if row["TYPEQU"] == "B101" or row["TYPEQU"] == "B102":
                        if (
                            row["TYPEQU"] != ""
                            and row["DCIRIS"] != ""
                            and row["DEPCOM"] != ""
                            and row["LAMBERT_X"] != ""
                            and row["LAMBERT_Y"] != ""
                        ):
                            copy.write_row(
                                [
                                    row["TYPEQU"],
                                    row["DCIRIS"],
                                    row["DEPCOM"],
                                    f"""Point({row["LAMBERT_X"]} {row["LAMBERT_Y"]})""",
                                ]
                            )

            with cur.copy(
                "COPY proj.addresses (code_insee, departement, point_2154) FROM STDIN"
            ) as copy:
                for row in tqdm(get_address_rows(), desc="Copying addresses"):
                    if (
                        row["code_insee"]
                        and row["x"]
                        and row["y"]
                        and row["code_postal"]
                    ):
                        if row["code_postal"][:2] == "97":
                            dept = row["code_postal"][:3]
                        else:
                            dept = row["code_postal"][:2]
                        copy.write_row(
                            [
                                row["code_insee"],
                                dept,
                                f"""Point({row["x"]} {row["y"]})""",
                            ]
                        )

        conn.commit()
